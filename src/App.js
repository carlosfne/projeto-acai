import './App.css';

import logo from './assets/images/logo.png'

function App() {
  return (
    <div className="App">
      <img src={logo} alt='logo'/>
      <input className='input-app' placeholder='Digite seu nome' />
      <input className='input-app' placeholder='Digite sua senha' />
      <a>Esqueci minha senha</a>
      <button>Entrar</button>
    </div>
  );
}

export default App;
